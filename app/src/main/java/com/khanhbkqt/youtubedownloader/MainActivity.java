package com.khanhbkqt.youtubedownloader;

import android.app.DownloadManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.khanhbkqt.youtubedownloader.core.RxYoutube;
import com.khanhbkqt.youtubedownloader.core.entity.FmtStreamMap;
import com.khanhbkqt.youtubedownloader.utils.DownloadFileFromURL;

import java.util.List;

import rx.functions.Action1;

public class MainActivity extends AppCompatActivity {

    private DownloadManager downloadManager;

    private String mVideoId = "hoXvwWG6wBs";

    private Action1<List<FmtStreamMap>> resultAction = new Action1<List<FmtStreamMap>>() {
        @Override
        public void call(List<FmtStreamMap> fmtStreamMaps) {
            final FmtStreamMap fmtStreamMap = fmtStreamMaps.get(0);
            RxYoutube.parseDownloadUrl(fmtStreamMap, new Action1<String>() {
                @Override
                public void call(String s) {
                    String fileName = fmtStreamMap.title + "." + fmtStreamMap.extension;
                    Uri uri = Uri.parse(s);
                    // Link download
                    Log.d("LINK", uri.toString());

                    new DownloadFileFromURL().execute(uri.toString());

                    DownloadManager.Request request = new DownloadManager.Request(uri);
                    request.setDestinationInExternalFilesDir(MainActivity.this,
                            Environment.DIRECTORY_MOVIES, fileName);
                    // downloadManager.enqueue(request);
                }
            });
        }
    };
    private Action1<Throwable> errorAction = new Action1<Throwable>() {
        @Override
        public void call(Throwable throwable) {
            throwable.printStackTrace();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);

        RxYoutube.fetchYoutube(mVideoId, resultAction, errorAction);
    }
}
