/*
 * Copyright (C) 2015 Baidu, Inc. All Rights Reserved.
 */
package com.khanhbkqt.youtubedownloader.core;

public enum ResolutionNote {
        HD, MHD, LHD, XLHD
    }